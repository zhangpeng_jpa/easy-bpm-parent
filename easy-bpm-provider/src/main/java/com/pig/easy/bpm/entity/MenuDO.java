package com.pig.easy.bpm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author pig
 * @since 2020-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("bpm_menu")
public class MenuDO implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 编号
     */
    @TableId(value = "menu_id", type = IdType.AUTO)
    private Long menuId;

    /**
     * 资源编码
     */
    private String menuCode;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 菜单编码
     */
    private String menuIcon;

    /**
     * 菜单URL
     */
    private String menuUrl;

    /**
     * 菜单类型 menu: 菜单
     */
    private String menuType;

    /**
     * 上级编号 0为 1级
     */
    private Long parentId;

    /**
     * 租户编号
     */
    private String tenantId;

    /**
     * 组件地址
     */
    private String component;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 是否隐藏 0 不隐藏 1 隐藏
     */
    private Integer hidden;


    private String meta;


    private Integer alwaysShow;


    private String redirect;


    /**
     * 状态 0 失效 1 有效
     */
    private Integer validState;

    /**
     * 操作者工号
     */
    private Long operatorId;

    /**
     * 操作人名称
     */
    private String operatorName;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
