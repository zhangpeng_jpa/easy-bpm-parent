package com.pig.easy.bpm.service.impl;

import com.pig.easy.bpm.entity.RoleGroupToRoleDO;
import com.pig.easy.bpm.mapper.RoleGroupToRoleMapper;
import com.pig.easy.bpm.service.RoleGroupToRoleService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author pig
 * @since 2020-06-14
 */
@com.alibaba.dubbo.config.annotation.Service
public class RoleGroupToRoleServiceImpl extends BeseServiceImpl<RoleGroupToRoleMapper, RoleGroupToRoleDO> implements RoleGroupToRoleService {

    @Autowired
    RoleGroupToRoleMapper roleGroupToRoleMapper;
}
