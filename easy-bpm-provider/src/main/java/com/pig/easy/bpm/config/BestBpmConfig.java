package com.pig.easy.bpm.config;

import com.pig.easy.bpm.event.BestBpmEventDispatcher;
import com.pig.easy.bpm.event.BestBpmEventDispatcherImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * todo: besr bpm 默认配置文件
 *
 * @author : pig
 * @date : 2020/5/20 16:11
 */
@Configuration
public class BestBpmConfig {

    /**
     * 功能描述: init bestBpmEventDispatcher
     *
     *
     * @return : com.pig.easy.bpm.event.BestBpmEventDispatcher
     * @author : pig
     * @date : 2020/5/20 18:11
     */
    @Bean
    public BestBpmEventDispatcher bestBpmEventDispatcher() {
        return new BestBpmEventDispatcherImpl();
    }


}
