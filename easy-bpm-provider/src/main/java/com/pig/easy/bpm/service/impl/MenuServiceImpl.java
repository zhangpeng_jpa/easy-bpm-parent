package com.pig.easy.bpm.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.constant.BpmConstant;
import com.pig.easy.bpm.dto.request.MenuQueryDTO;
import com.pig.easy.bpm.dto.request.MenuSaveOrUpdateDTO;
import com.pig.easy.bpm.dto.response.MenuDTO;
import com.pig.easy.bpm.dto.response.MenuTreeDTO;
import com.pig.easy.bpm.entity.MenuDO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.mapper.MenuMapper;
import com.pig.easy.bpm.service.MenuService;
import com.pig.easy.bpm.utils.BeanUtils;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.CommonUtils;
import com.pig.easy.bpm.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author pig
 * @since 2020-07-09
 */
@Service
public class MenuServiceImpl extends BeseServiceImpl<MenuMapper, MenuDO> implements MenuService {

    @Autowired
    MenuMapper mapper;

    @Override
    public Result<PageInfo<MenuDTO>> getListByCondition(MenuQueryDTO param) {

        if (param == null) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        int pageIndex = CommonUtils.evalInt(param.getPageIndex(), DEFAULT_PAGE_INDEX);
        int pageSize = CommonUtils.evalInt(param.getPageSize(), DEFAULT_PAGE_SIZE);

        if(StringUtils.isEmpty(param.getMenuType())){
            param.setMenuName(BpmConstant.MENE_TYPE_MENU);
        }

        PageHelper.startPage(pageIndex, pageSize);

        List<MenuDTO> list = mapper.getListByCondition(param);
        if (list == null) {
            list = new ArrayList<>();
        }
        PageInfo<MenuDTO> pageInfo = new PageInfo<>(list);
        return Result.responseSuccess(pageInfo);
    }

    @Override
    public Result<List<MenuTreeDTO>> getMenuTree(String tenantId, Long parentId) {

        BestBpmAsset.isAssetEmpty(tenantId);
        if (parentId == null) {
            parentId = 0L;
        }
        List<MenuDO> list = mapper.selectList(new QueryWrapper<>(
                MenuDO.builder().tenantId(tenantId).validState(VALID_STATE).build()).orderByAsc("parent_id","sort"));
        if(list == null){
            list = new ArrayList<>();
        }
        List<MenuTreeDTO> menuList = new ArrayList<>();
        for(MenuDO menuDO : list){
            menuList.add(BeanUtils.switchToDTO(menuDO,MenuTreeDTO.class));
        }
        menuList = makeMenuTree(menuList,parentId);
        return Result.responseSuccess(menuList);
    }

    private List<MenuTreeDTO> makeMenuTree(List<MenuTreeDTO> treeList, Long parentId) {

        List<MenuTreeDTO> tempTreeList = new ArrayList<>();
        for(MenuTreeDTO menuTreeDTO : treeList){
            if(parentId.equals(menuTreeDTO.getParentId())) {
                tempTreeList.add(findChildren(menuTreeDTO, treeList, menuTreeDTO.getMenuId()));
            }
        }

        return tempTreeList;
    }

    private static MenuTreeDTO findChildren(MenuTreeDTO treeDTO,List<MenuTreeDTO> treeList, Long parentId) {

        List<MenuTreeDTO> children = treeList.stream().filter(x -> parentId.equals(x.getParentId())).collect(Collectors.toList());
        children.forEach(x ->
                {
                    if (treeDTO.getChildren() == null) {
                        treeDTO.setChildren(new ArrayList<>());
                    }
                    treeDTO.getChildren().add(findChildren(x,treeList, x.getMenuId()));
                }
        );

        return treeDTO;
    }

    @Override
    public Result<Integer> insertMenu(MenuSaveOrUpdateDTO param) {

        if (param == null) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        MenuDO temp = BeanUtils.switchToDO(param, MenuDO.class);
        Integer num = mapper.insert(temp);
        return Result.responseSuccess(num);
    }

    @Override
    public Result<Integer> updateMenu(MenuSaveOrUpdateDTO param) {

        if (param == null) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        MenuDO temp = BeanUtils.switchToDO(param, MenuDO.class);
        Integer num = mapper.updateById(temp);
        return Result.responseSuccess(num);
    }

    @Override
    public Result<Integer> deleteMenu(MenuSaveOrUpdateDTO param) {

        if (param == null) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        MenuDO temp = BeanUtils.switchToDO(param, MenuDO.class);
        temp.setValidState(INVALID_STATE);
        Integer num = mapper.updateById(temp);
        return Result.responseSuccess(num);
    }

}
