package com.pig.easy.bpm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pig.easy.bpm.dto.response.ItemDTO;
import com.pig.easy.bpm.dto.response.RoleGroupDTO;
import com.pig.easy.bpm.entity.RoleGroupDO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.mapper.RoleGroupMapper;
import com.pig.easy.bpm.service.RoleGroupService;
import com.pig.easy.bpm.utils.BeanUtils;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 角色组 服务实现类
 * </p>
 *
 * @author pig
 * @since 2020-06-14
 */
@com.alibaba.dubbo.config.annotation.Service
public class RoleGroupServiceImpl extends BeseServiceImpl<RoleGroupMapper, RoleGroupDO> implements RoleGroupService {

    @Autowired
    RoleGroupMapper roleGroupMapper;

    @Override
    public Result<RoleGroupDTO> getRoleGroupByCode(String roleGroupCode) {

        BestBpmAsset.isEmpty(roleGroupCode,"roleGroupCode is not allow empty");

        RoleGroupDO roleGroupDO = roleGroupMapper.selectOne(new QueryWrapper<>(RoleGroupDO.builder().roleGroupCode(roleGroupCode).validState(VALID_STATE).build()));
        if(roleGroupDO == null){
            return Result.responseError(EntityError.DATA_NOT_FOUND_ERROR);
        }
        RoleGroupDTO roleGroupDTO = BeanUtils.switchToDTO(roleGroupDO,RoleGroupDTO.class);
        return Result.responseSuccess(roleGroupDTO);
    }

    @Override
    public Result<RoleGroupDTO> getRoleGroupById(Long roleGroupId) {

        BestBpmAsset.isEmpty(roleGroupId,"roleGroupId is not allow empty");
        RoleGroupDO roleGroupDO = roleGroupMapper.selectOne(new QueryWrapper<>(RoleGroupDO.builder().roleGroupId(roleGroupId).validState(VALID_STATE).build()));
        if(roleGroupDO == null){
            return Result.responseError(EntityError.DATA_NOT_FOUND_ERROR);
        }
        RoleGroupDTO roleGroupDTO = BeanUtils.switchToDTO(roleGroupDO,RoleGroupDTO.class);
        return Result.responseSuccess(roleGroupDTO);
    }

    @Override
    public Result<List<ItemDTO>> getRoleGroupDict(String tenantId) {

        BestBpmAsset.isEmpty(tenantId,"tenantId is not allow empty");
        List<RoleGroupDO> roleGroupDOList = roleGroupMapper.selectList(new QueryWrapper<>(RoleGroupDO.builder().tenantId(tenantId).validState(VALID_STATE).build()));
        List<ItemDTO> result = new ArrayList<>();
        ItemDTO item = null;
        for (RoleGroupDO roleGroupDO : roleGroupDOList) {
            item = new ItemDTO();
            item.setLabel(roleGroupDO.getRoleGroupName());
            item.setValue(roleGroupDO.getRoleGroupCode());
            result.add(item);
        }
        
        return Result.responseSuccess(result);
    }
}
