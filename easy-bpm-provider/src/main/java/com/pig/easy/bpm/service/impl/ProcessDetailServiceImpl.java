package com.pig.easy.bpm.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.constant.BpmConstant;
import com.pig.easy.bpm.dto.request.AddNodeInfoDTO;
import com.pig.easy.bpm.dto.request.ProcessDetailReqDTO;
import com.pig.easy.bpm.dto.request.ProcessPublishDTO;
import com.pig.easy.bpm.dto.request.ProcessReqDTO;
import com.pig.easy.bpm.dto.response.ProcessDTO;
import com.pig.easy.bpm.dto.response.ProcessDetailDTO;
import com.pig.easy.bpm.entity.NodeDO;
import com.pig.easy.bpm.entity.ProcessDetailDO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.mapper.ProcessDetailMapper;
import com.pig.easy.bpm.service.NodeService;
import com.pig.easy.bpm.service.ProcessDetailService;
import com.pig.easy.bpm.service.ProcessService;
import com.pig.easy.bpm.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.flowable.bpmn.converter.BpmnXMLConverter;
import org.flowable.bpmn.model.BpmnModel;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.impl.persistence.entity.DeploymentEntityImpl;
import org.flowable.engine.impl.persistence.entity.ProcessDefinitionEntityImpl;
import org.flowable.engine.repository.Deployment;
import org.flowable.validation.ProcessValidator;
import org.flowable.validation.ProcessValidatorFactory;
import org.flowable.validation.ValidationError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author pig
 * @since 2020-06-06
 */
@Slf4j
@com.alibaba.dubbo.config.annotation.Service
public class ProcessDetailServiceImpl extends BeseServiceImpl<ProcessDetailMapper, ProcessDetailDO> implements ProcessDetailService {

    @Autowired
    ProcessDetailMapper processDetailMapper;

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    ProcessService processService;
    @Reference
    NodeService nodeService;

    @Override
    public Result<ProcessDetailDTO> getProcessDetailByProcessId(Long processId) {

        if (CommonUtils.evalLong(processId) < 0) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        ProcessDetailDO processDetail = ProcessDetailDO.builder()
                .processId(processId)
                .mainVersion(BpmConstant.PROCESS_DERTAIL_IS_MAIN_VERSION)
                .validState(VALID_STATE)
                .build();
        QueryWrapper<ProcessDetailDO> entityWrapper = new QueryWrapper<>(processDetail);
        processDetail = processDetailMapper.selectOne(entityWrapper);

        ProcessDetailDTO processDetailDTO = BeanUtils.switchToDTO(processDetail, ProcessDetailDTO.class);
        return Result.responseSuccess(processDetailDTO);
    }

    @Override
    public Result<ProcessDetailDTO> getProcessDetailByDefinitionId(String definitionId) {
        if (StringUtils.isEmpty(definitionId)) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        ProcessDetailDO processDetail = ProcessDetailDO.builder()
                .definitionId(definitionId)
                .build();
        QueryWrapper<ProcessDetailDO> entityWrapper = new QueryWrapper<>(processDetail);
        processDetail = processDetailMapper.selectOne(entityWrapper);

        ProcessDetailDTO processDetailDTO = BeanUtils.switchToDTO(processDetail, ProcessDetailDTO.class);
        return Result.responseSuccess(processDetailDTO);
    }

    @Override
    public Result<ProcessDetailDTO> getProcessDetailById(Long processDetailId) {

        if (CommonUtils.evalLong(processDetailId) < 0) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        ProcessDetailDO processDetailDO = new ProcessDetailDO();
        processDetailDO.setProcessDetailId(processDetailId);
        processDetailDO = processDetailMapper.selectOne(new QueryWrapper<>(processDetailDO));

        if (processDetailDO == null) {
            return Result.responseError(EntityError.DATA_NOT_FOUND_ERROR);
        }
        ProcessDetailDTO processDetailDTO = BeanUtils.switchToDTO(processDetailDO, ProcessDetailDTO.class);
        return Result.responseSuccess(processDetailDTO);
    }

    @Override
    public Result<PageInfo> getListByCondiction(ProcessDetailReqDTO processDetailReqDTO) {

        if (processDetailReqDTO == null) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        int pageIndex = CommonUtils.evalInt(processDetailReqDTO.getPageIndex(), DEFAULT_PAGE_INDEX);
        int pageSize = CommonUtils.evalInt(processDetailReqDTO.getPageSize(), DEFAULT_PAGE_SIZE);

        ProcessDetailDO processDetail = BeanUtils.switchToDO(processDetailReqDTO, ProcessDetailDO.class);
        processDetail.setValidState(VALID_STATE);
        PageHelper.startPage(pageIndex, pageSize);
        List<ProcessDetailDTO> result = processDetailMapper.getListByCondition(processDetail);
        if (result == null) {
            result = new ArrayList<>();
        }
        PageInfo<ProcessDetailDTO> pageInfo = new PageInfo<>(result);
        return Result.responseSuccess(pageInfo);
    }

    @Override
    public Result<Integer> updateProcessDetail(ProcessDetailReqDTO processDetailReqDTO) {

        if (processDetailReqDTO == null
                || CommonUtils.evalLong(processDetailReqDTO.getProcessDetailId()) < 0) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        if (!StringUtils.isEmpty(processDetailReqDTO.getProcessXml())) {
            Result<Boolean> result = checkProcessXml(processDetailReqDTO.getProcessXml());
            if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
                return Result.responseError(result.getEntityError());
            }
        }

        ProcessDetailDO processDetail = BeanUtils.switchToDO(processDetailReqDTO, ProcessDetailDO.class);
        processDetail.setUpdateTime(LocalDateTime.now());

        return Result.responseSuccess(processDetailMapper.updateById(processDetail));
    }

    @Override
    public Result<Integer> insertProcessDetail(ProcessDetailReqDTO processDetailReqDTO) {
        if (processDetailReqDTO == null
                || CommonUtils.evalLong(processDetailReqDTO.getProcessId()) < 0
                || StringUtils.isEmpty(processDetailReqDTO.getTenantId())) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        if (!StringUtils.isEmpty(processDetailReqDTO.getProcessXml())) {
            Result<Boolean> result = checkProcessXml(processDetailReqDTO.getProcessXml());
            if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
                return Result.responseError(result.getEntityError());
            }
        }
        ProcessDetailDO processDetail = BeanUtils.switchToDO(processDetailReqDTO, ProcessDetailDO.class);
        int num = processDetailMapper.insert(processDetail);
        return Result.responseSuccess(processDetail.getProcessDetailId().intValue());
    }

    @Override
    public Result<Boolean> checkProcessXml(String processXml) {

        if (StringUtils.isEmpty(processXml)) {
            return Result.responseSuccess(false);
        }
        BpmnXMLConverter bpmnXMLConverter = new BpmnXMLConverter();
        byte[] bytes = processXml.getBytes();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        XMLInputFactory xif = XMLInputFactory.newInstance();
        InputStreamReader in = null;
        try {
            in = new InputStreamReader(inputStream, "UTF-8");
            XMLStreamReader xtr = xif.createXMLStreamReader(in);
            BpmnModel bpmnModel = bpmnXMLConverter.convertToBpmnModel(xtr);
            //验证组装bpmnmodel是否正确
            ProcessValidator defaultProcessValidator = new ProcessValidatorFactory().createDefaultProcessValidator();
            List<ValidationError> validate = defaultProcessValidator.validate(bpmnModel);
            if (validate.size() > 0) {
                log.error("checkProcessXml error {}", JSONArray.toJSONString(validate));
                return Result.responseError(new EntityError(EntityError.SYSTEM_ERROR.getCode(), JSONArray.toJSONString(validate)));
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Result.responseError(new EntityError(EntityError.SYSTEM_ERROR.getCode(), e.getMessage()));
        } catch (XMLStreamException e) {
            return Result.responseError(new EntityError(EntityError.SYSTEM_ERROR.getCode(), e.getMessage()));
        }
        return Result.responseSuccess(true);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result<Boolean> processPublish(ProcessPublishDTO processPublishDTO) {

        BestBpmAsset.isAssetEmpty(processPublishDTO);
        BestBpmAsset.isAssetEmpty(processPublishDTO.getTenantId());
        BestBpmAsset.isAssetEmpty(processPublishDTO.getProcessKey());
        BestBpmAsset.isAssetEmpty(processPublishDTO.getProcessDetailId());
        BestBpmAsset.isAssetEmpty(processPublishDTO.getProcessXml());

        ProcessDetailDO processDetailDO = processDetailMapper.selectById(processPublishDTO.getProcessDetailId());
        if (processDetailDO == null) {
            return Result.responseError(EntityError.DATA_NOT_FOUND_ERROR);
        }

        /* 校验流程格式 */
        Result<Boolean> result = checkProcessXml(processPublishDTO.getProcessXml());
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return Result.responseError(result.getEntityError());
        }

        /* 校验processKey */
        Result<ProcessDTO> result1 = processService.getProcessByProcessKey(processPublishDTO.getProcessKey());
        if (result1.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return Result.responseError(result1.getEntityError());
        }

        ProcessDTO processDTO = result1.getData();
        Deployment deployment = null;
        try {
            deployment = repositoryService.createDeployment()
                    .name(processDTO.getProcessName())
                    .key(processPublishDTO.getProcessKey())
                    .tenantId(processPublishDTO.getTenantId())
                    .addBytes(processPublishDTO.getProcessKey() + ".bpmn20.xml", processPublishDTO.getProcessXml().getBytes("UTF-8"))
                    .deploy();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Result.responseError(new EntityError(EntityError.SYSTEM_ERROR.getCode(), e.getMessage()));
        }

        ProcessDetailDO tempProcessDetail = processDetailDO;
        tempProcessDetail.setDefinitionId(((DeploymentEntityImpl) deployment).getDeployedArtifacts(ProcessDefinitionEntityImpl.class).get(0).getId());
        if (CommonUtils.evalLong(processPublishDTO.getOperatorId()) > 0) {
            tempProcessDetail.setOperatorId(processPublishDTO.getOperatorId());
        }
        if (StringUtils.isEmpty(processPublishDTO.getOperatorName())) {
            tempProcessDetail.setOperatorName(processPublishDTO.getOperatorName());
        }

        // 保存 nodeList
        Result<Boolean> result3 = saveNodeList(processPublishDTO.getProcessXml(),tempProcessDetail.getDefinitionId(), processPublishDTO.getProcessKey(),processPublishDTO.getOperatorId(),processPublishDTO.getOperatorName());
        if (result3.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return Result.responseError(result3.getEntityError());
        }
        // 如果是已发布,则重新创建一个
        if (processDetailDO.getPublishStatus() == BpmConstant.PROCESS_DETAIL_PUBLISH) {
            tempProcessDetail.setProcessDetailId(null);
            tempProcessDetail.setPublishStatus(BpmConstant.PROCESS_DETAIL_PUBLISH);
            tempProcessDetail.setMainVersion(BpmConstant.PROCESS_DERTAIL_NOT_MAIN_VERSION);
            processDetailMapper.insert(tempProcessDetail);
        }

        if (processDetailDO.getPublishStatus() == BpmConstant.PROCESS_DETAIL_UNPUBLISH) {
            tempProcessDetail.setPublishStatus(BpmConstant.PROCESS_DETAIL_PUBLISH);
            processDetailMapper.updateById(tempProcessDetail);
        }

        // 判断 流程表 发布状态 如果为 未发布，则更新为已发布
        if (processDTO.getProcessStatus() == BpmConstant.PROCESS_DETAIL_UNPUBLISH) {
            ProcessReqDTO processReqDTO = new ProcessReqDTO();
            processReqDTO.setProcessStatus(BpmConstant.PROCESS_DETAIL_PUBLISH);
            processReqDTO.setProcessKey(processPublishDTO.getProcessKey());
            Result<Integer> result2 = processService.updateProcessByProcessKey(processReqDTO);
            if (result2.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
                return Result.responseError(result2.getEntityError());
            }
        }

        return Result.responseSuccess(true);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result<Boolean> updateDefaultVersion(Long processId, Long processDetailId, Long operatorId, String operatorName) {

        BestBpmAsset.isAssetEmpty(processId);
        BestBpmAsset.isAssetEmpty(processDetailId);
        BestBpmAsset.isAssetEmpty(operatorId);
        BestBpmAsset.isAssetEmpty(operatorName);

        Result<ProcessDetailDTO> result = getProcessDetailById(processDetailId);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return Result.responseError(result.getEntityError());
        }
        ProcessDetailDTO processDetailDTO = result.getData();

        Result<ProcessDTO> result1 = processService.getProcessById(processId);
        if (result1.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return Result.responseError(result1.getEntityError());
        }
        ProcessDTO processDTO = result1.getData();

        // 未发布流程不允许设置为默认主版本
        if (processDetailDTO.getPublishStatus() == BpmConstant.PROCESS_DETAIL_UNPUBLISH) {
            return Result.responseError(EntityError.UNPUBLISH_PROCESS_DETAIL_DEFAULT_ERROR);
        }

        // 如果当前版本为 主版本，并且 流程信息关联的为当前详细编号，则跳过
        if (processDetailDTO.getMainVersion() == BpmConstant.PROCESS_DERTAIL_IS_MAIN_VERSION
                && processDTO.getProcessDetailId().equals(processDetailDTO.getProcessDetailId())) {
            return Result.responseSuccess(true);
        }

        ProcessDetailDO tempProcessDetail = new ProcessDetailDO();
        tempProcessDetail.setMainVersion(BpmConstant.PROCESS_DERTAIL_NOT_MAIN_VERSION);
        tempProcessDetail.setOperatorName(operatorName);
        tempProcessDetail.setOperatorId(operatorId);
        tempProcessDetail.setUpdateTime(LocalDateTime.now());
        processDetailMapper.update(tempProcessDetail, new QueryWrapper<ProcessDetailDO>()
                .eq("process_id", processId).ne("process_detail_id", processDetailId));

        ProcessDetailReqDTO processDetailReqDTO = new ProcessDetailReqDTO();
        processDetailReqDTO.setProcessDetailId(processDetailId);
        processDetailReqDTO.setOperatorId(operatorId);
        processDetailReqDTO.setOperatorName(operatorName);
        processDetailReqDTO.setMainVersion(BpmConstant.PROCESS_DERTAIL_IS_MAIN_VERSION);
        Result<Integer> result2 = updateProcessDetail(processDetailReqDTO);
        if (result2.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return Result.responseError(result2.getEntityError());
        }

        ProcessReqDTO processReqDTO = BeanUtils.switchToDTO(processDTO, ProcessReqDTO.class);
        processReqDTO.setProcessDetailId(processDetailId);
        Result<Integer> result3 = processService.updateProcessByProcessKey(processReqDTO);
        if (result3.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return Result.responseError(result3.getEntityError());
        }

        return Result.responseSuccess(true);
    }

    @Override
    public Result<Boolean> saveNodeList(String processXml, String definitionId,String processKey,Long operatorId,String operatorName) {

        Result<ProcessDTO> result1 = processService.getProcessByProcessKey(processKey);
        if (result1.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return Result.responseError(result1.getEntityError());
        }
        ProcessDTO processDTO = result1.getData();

        BpmnXMLConverter bpmnXMLConverter = new BpmnXMLConverter();
        byte[] bytes = new byte[0];
        try {
            bytes = processXml.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Result.responseError(EntityError.SYSTEM_ERROR);
        }
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        XMLInputFactory xif = XMLInputFactory.newInstance();
        InputStreamReader in = null;

        try {
            in = new InputStreamReader(inputStream, "UTF-8");
            XMLStreamReader xtr = xif.createXMLStreamReader(in);
            BpmnModel bpmnModel = bpmnXMLConverter.convertToBpmnModel(xtr);
            List<NodeDO> nodeDOList = FlowElementUtils.bpmnModelToNodeDOList(bpmnModel);
            List<AddNodeInfoDTO> addNodeInfoDTOS = new ArrayList<>();
            AddNodeInfoDTO nodeInfoDTO = null;
            for (NodeDO nodeDO : nodeDOList) {
                nodeInfoDTO = BeanUtils.switchToDTO(nodeDO,AddNodeInfoDTO.class);
                nodeInfoDTO.setProcessId(processDTO.getProcessId());
                nodeInfoDTO.setProcessKey(processDTO.getProcessKey());
                nodeInfoDTO.setTenantId(processDTO.getTenantId());
                nodeInfoDTO.setValidState(VALID_STATE);
                if (CommonUtils.evalLong(operatorId) > 0) {
                    nodeInfoDTO.setOperatorId(operatorId.intValue());
                }
                if (StringUtils.isEmpty(operatorName)) {
                    nodeInfoDTO.setOperatorName(operatorName);
                }
                addNodeInfoDTOS.add(nodeInfoDTO);
            }

            Result<Integer> result2 = nodeService.batchInsertOrUpdateNodeInfo(addNodeInfoDTOS, definitionId);
            if (result2.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
                return Result.responseError(result2.getEntityError());
            }


        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.responseError(EntityError.SYSTEM_ERROR);
        }
        return Result.responseSuccess(true);
    }
}
