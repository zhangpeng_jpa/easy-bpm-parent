package com.pig.easy.bpm.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.constant.BpmConstant;
import com.pig.easy.bpm.dto.request.DictQueryDTO;
import com.pig.easy.bpm.dto.request.DictUpdateDTO;
import com.pig.easy.bpm.dto.response.DictDTO;
import com.pig.easy.bpm.dto.response.DictItemDTO;
import com.pig.easy.bpm.dto.response.ItemDTO;
import com.pig.easy.bpm.entity.DictDO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.mapper.DictMapper;
import com.pig.easy.bpm.service.DictItemService;
import com.pig.easy.bpm.service.DictService;
import com.pig.easy.bpm.utils.BeanUtils;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.CommonUtils;
import com.pig.easy.bpm.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author pig
 * @since 2020-05-28
 */
@Service
public class DictServiceImpl extends BeseServiceImpl<DictMapper, DictDO> implements DictService {

    @Autowired
    DictMapper dictMapper;
    @Reference
    DictItemService dictItemService;

    @Override
    public Result<PageInfo> getListByCondition(DictQueryDTO dictQueryDTO) {

        BestBpmAsset.isAssetEmpty(dictQueryDTO);
        BestBpmAsset.isAssetEmpty(dictQueryDTO.getTenantId());
        int pageIndex = CommonUtils.evalInt(dictQueryDTO.getPageIndex(), DEFAULT_PAGE_INDEX);
        int pageSize = CommonUtils.evalInt(dictQueryDTO.getPageSize(), DEFAULT_PAGE_SIZE);

        PageHelper.startPage(pageIndex, pageSize);
        List<DictDTO> result = dictMapper.getListByCondition(dictQueryDTO);
        if (result == null) {
            result = new ArrayList<>();
        }

        PageInfo<DictDTO> pageInfo = new PageInfo<>(result);
        return Result.responseSuccess(pageInfo);
    }

    @Override
    public Result<DictDTO> getDictByDictCode(String dictCode) {

        BestBpmAsset.isAssetEmpty(dictCode);
        DictDO dictDO = dictMapper.selectOne(new QueryWrapper<>(DictDO.builder().dictCode(dictCode).build()));
        if (dictDO == null) {
            return Result.responseError(EntityError.DATA_NOT_FOUND_ERROR);
        }
        DictDTO dictDTO = BeanUtils.switchToDTO(dictDO,DictDTO.class);
        return Result.responseSuccess(dictDTO);
    }

    @Override
    public Result<Integer> insertOrUpdateDict(DictUpdateDTO dictUpdateDTO) {

        BestBpmAsset.isAssetEmpty(dictUpdateDTO);
        BestBpmAsset.isAssetEmpty(dictUpdateDTO.getDictCode());

        DictDO dictDO1 = dictMapper.selectOne(new QueryWrapper<>(DictDO.builder().dictCode(dictUpdateDTO.getDictCode()).build()));
        DictDO dictDO = BeanUtils.switchToDO(dictUpdateDTO, DictDO.class);
        if (CommonUtils.evalInt(dictUpdateDTO.getCurrentUserId()) > 0) {
            dictDO.setOperatorId(dictUpdateDTO.getCurrentUserId());
        }
        if (!StringUtils.isEmpty(dictUpdateDTO.getCurrentRealName())) {
            dictDO.setOperatorName(dictUpdateDTO.getCurrentRealName());
        }
        dictDO.setUpdateTime(LocalDateTime.now());
        int num = 0;
        if (dictDO1 == null) {
            // 默认 key 格式   租户 +"-" + key
            if (!dictDO.getDictCode().startsWith((dictDO.getTenantId()))) {
                dictDO.setDictCode(dictDO.getTenantId()
                        + BpmConstant.COMMON_CODE_SEPARATION_CHARACTER
                        + "dict"
                        + BpmConstant.COMMON_CODE_SEPARATION_CHARACTER
                        + dictDO.getDictCode());
            }

            num = dictMapper.insert(dictDO);
        } else {
            num = dictMapper.update(dictDO, new QueryWrapper<>(DictDO.builder().dictCode(dictUpdateDTO.getDictCode()).build()));
        }
        return Result.responseSuccess(num);
    }

    @Override
    public Result<List<ItemDTO>> getDictListByDictCode(String dictCode) {

       if(StringUtils.isEmpty(dictCode)){
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
       }
        Result<List<DictItemDTO>> result = dictItemService.getListByDictCode(dictCode);
        if(result.getEntityError().getCode() != EntityError.SUCCESS.getCode()){
            return Result.responseError(result.getEntityError());
       }
        List<DictItemDTO> list = result.getData();

        List<ItemDTO> itemList = new ArrayList<>();
        ItemDTO itemDTO = null;
        for (DictItemDTO dictItem : list) {
            itemDTO = new ItemDTO();
            itemDTO.setLabel(dictItem.getItemText());
            try {
                itemDTO.setValue(Integer.valueOf(dictItem.getItemValue()));
            } catch (Exception e) {
                itemDTO.setValue(dictItem.getItemValue());
            }
            itemList.add(itemDTO);
        }
        return Result.responseSuccess(itemList);
    }

    @Override
    public Result<List<ItemDTO>> getDict(DictQueryDTO dictQueryDTO) {

        if(dictQueryDTO == null){
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        DictDO tempDict = BeanUtils.objectToBean(dictQueryDTO, DictDO.class);
        DictDO dictDO = dictMapper.selectOne(new QueryWrapper<>(tempDict));
        if (dictDO == null) {
            return Result.responseError(EntityError.DATA_NOT_FOUND_ERROR);
        }
        Result<List<ItemDTO>> result = getDictListByDictCode(dictDO.getDictCode());
        if(result.getEntityError().getCode() != EntityError.SUCCESS.getCode()){
            return Result.responseError(result.getEntityError());
        }
        return Result.responseSuccess(result.getData());
    }
}
