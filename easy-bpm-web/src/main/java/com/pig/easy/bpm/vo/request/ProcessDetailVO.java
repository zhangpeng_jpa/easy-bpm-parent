package com.pig.easy.bpm.vo.request;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/17 16:57
 */
@Data
@ToString
public class ProcessDetailVO implements Serializable {

    private static final long serialVersionUID = -1015518386352453921L;

    private Long processDetailId;

    private String tenantId;

    private Long processId;

    private String processXml;

    private String applyTitleRule;

    private Date applyDueDate;

    private String remarks;

    private Integer validState;

    private Long operatorId;

    private String operatorName;

    private Integer pageIndex;

    private Integer pageSize;

    private Integer publishStatus;

    private Integer mainVersion;

    private String definitionId;

    private Integer autoCompleteFirstNode;

}
