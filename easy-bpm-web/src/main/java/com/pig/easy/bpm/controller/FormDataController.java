package com.pig.easy.bpm.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.FormDataService;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.JsonResult;
import com.pig.easy.bpm.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-05-28
 */
@RestController
@RequestMapping("/formData")
@Api(tags = "表单数据管理", value = "表单数据管理")
public class FormDataController extends BaseController {

    @Reference
    FormDataService formDataService;


    @ApiOperation(value = "根据申请编号获取表单数据", notes = "获取表单数据", produces = "application/json")
    @PostMapping("/getFormDataByApplyId/{applyId}")
    public JsonResult getFormDataByApplyId(@ApiParam(required = true, name = "申请编号", value = "applyId", example = "1") @PathVariable("applyId") Long applyId) {

        BestBpmAsset.isAssetEmpty(applyId);

        Result<Map<String, Object>> result = formDataService.getFormDataByApplyId(applyId);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "根据流程实例编号获取表单数据", notes = "获取表单数据", produces = "application/json")
    @PostMapping("/getFormDataByProcInstId/{procInstId}")
    public JsonResult getFormDataByApplyId(@ApiParam(required = true, name = "申请编号", value = "procInstId", example = "1") @PathVariable("procInstId") String procInstId) {

        BestBpmAsset.isAssetEmpty(procInstId);

        Result<Map<String, Object>> result = formDataService.getFormDataByProcInstId(procInstId);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }
}

