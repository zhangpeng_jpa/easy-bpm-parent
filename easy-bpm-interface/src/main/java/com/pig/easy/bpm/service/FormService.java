package com.pig.easy.bpm.service;

import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.FormReqDTO;
import com.pig.easy.bpm.dto.response.DynamicFormDataDTO;
import com.pig.easy.bpm.dto.response.FormDTO;
import com.pig.easy.bpm.utils.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pig
 * @since 2020-05-28
 */
public interface FormService{

    Result<PageInfo> getListByCondition(FormReqDTO formReqDTO);

    Result<FormDTO> getFormByFormKey(String formKey);

    Result<Integer> insertForm(FormReqDTO formReqDTO);

    Result<Integer> updateFormByFormKey(FormReqDTO formReqDTO);

    Result<DynamicFormDataDTO> getInitForm(String formKey);

}
