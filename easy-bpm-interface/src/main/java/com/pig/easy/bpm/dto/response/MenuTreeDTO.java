package com.pig.easy.bpm.dto.response;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/7/9 17:16
 */
@Data
@ToString
public class MenuTreeDTO  implements Serializable {

    private static final long serialVersionUID = -6242465531107993685L;

    /**
     * 编号
     */
    private Long menuId;

    /**
     * 资源编码
     */
    private String menuCode;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 菜单编码
     */
    private String menuIcon;


    /**
     * 租户编号
     */
    private String tenantId;
    /**
     * 菜单URL
     */
    private String menuUrl;

    /**
     * 菜单类型 menu: 菜单
     */
    private String menuType;

    /**
     * 上级编号 0为 1级
     */
    private Long parentId;


    /**
     * 组件地址
     */
    private String component;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 是否隐藏 0 不隐藏 1 隐藏
     */
    private Integer hidden;

    /**
     * 操作者工号
     */
    private Long operatorId;

    /**
     * 操作人名称
     */
    private String operatorName;

    private String meta;


    private Integer alwaysShow;


    private String redirect;

    private List<MenuTreeDTO> children;
}
