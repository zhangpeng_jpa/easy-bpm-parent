package com.pig.easy.bpm.dto.request;

import lombok.Data;
import lombok.ToString;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/26 17:47
 */
@Data
@ToString
public class DictUpdateDTO extends BaseRequestDTO{

    private static final long serialVersionUID = 674896436965449339L;

    private Long dictId;

    private String dictCode;

    private String dictName;

    private String tenantId;

    private String remark;

    private Integer validState;

}
