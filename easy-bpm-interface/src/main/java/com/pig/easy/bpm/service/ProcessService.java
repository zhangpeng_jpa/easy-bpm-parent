package com.pig.easy.bpm.service;

import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.ProcessReqDTO;
import com.pig.easy.bpm.dto.response.DynamicFormDataDTO;
import com.pig.easy.bpm.dto.response.ItemDTO;
import com.pig.easy.bpm.dto.response.ProcessDTO;
import com.pig.easy.bpm.dto.response.ProcessInfoDTO;
import com.pig.easy.bpm.utils.Result;

import java.util.List;

/**
 * <p>
 * 流程表 服务类
 * </p>
 *
 * @author pig
 * @since 2020-05-20
 */
public interface ProcessService {

    Result<PageInfo> getListByCondition(ProcessReqDTO processReqDTO);

    Result<ProcessDTO> getProcessById(Long processId);

    Result<ProcessDTO> getProcessByProcessKey(String processKey);

    Result<ProcessInfoDTO> getProcessWithDetailByProcessKey(String processKey);

    Result<Integer> insertProcess(ProcessReqDTO processReqDTO);

    Result<Integer> updateProcessByProcessKey(ProcessReqDTO processReqDTO);

    Result<DynamicFormDataDTO> getInitStartFormData(String processKey);

    Result<List<ItemDTO>> getProcessDict(String tenantId);
}
