package com.pig.easy.bpm.service;


import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.AddNodeInfoDTO;
import com.pig.easy.bpm.dto.request.NodeInfoReqDTO;
import com.pig.easy.bpm.dto.response.FlowUserTaskDTO;
import com.pig.easy.bpm.dto.response.NodeInfoDTO;
import com.pig.easy.bpm.utils.Result;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 流程节点表 服务类
 * </p>
 *
 * @author pig
 * @since 2020-06-22
 */
public interface NodeService  {

    Result<Integer> insertOrUpdateNodeInfo(AddNodeInfoDTO addNodeInfoDTO,String procDefId);

    Result<Integer> batchInsertOrUpdateNodeInfo(List<AddNodeInfoDTO> addNodeInfoDTOS,String procDefId);

    Result<List<NodeInfoDTO>> getNodeInfoListByCondition(NodeInfoReqDTO nodeInfoReqDTO);

    Result<PageInfo> getListByCondition(NodeInfoReqDTO nodeInfoReqDTO);

    Result<NodeInfoDTO> getNodeInfoByNodeIdAndDefinitionId(String nodeId ,String procDefId);

    Result<List<FlowUserTaskDTO>> getNextNodeList(String procDefId, String nodeId, Map<String, Object> dataMap,boolean multipleRecursion);

    Result<FlowUserTaskDTO> calcNodeUsers(String procDefId,String nodeId,Map<String,Object> dataMap);
}
